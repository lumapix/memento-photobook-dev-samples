/**
 * This is a example webhook endpoint using Express which demonstrates how
 * to handle events sent from Memento Photobook.
 */

const express = require("express");
// use body-parser to retrieve the raw body as a buffer
const bodyParser = require("body-parser");
const app = express();
const port = 3306;

let crypto;
try {
	// it's possible for Node.js to be built without including support for the crypto module
	// which is required to verify webhook signatures
	crypto = require('crypto');
} catch (err) {
	console.log('crypto support is disabled! aborting');
	process.exit(1)
}

app.post(
	"/webhook",
	bodyParser.raw({ type: "application/json" }),
	(request, response) => {
		const event = JSON.parse(request.body)
		// verify the request signature is valid
		// update the signing secret with the value in your publisher dashboard
		const secret = "whsec_mjsh7762TtCBhHWZFWI2Z8Ptnqk6Ghil";
		console.log(event)
		if (!isValidSignature(request, secret)) {
			console.log("invalid webhook signature. aborting");
			return response.status(422).json({ message: "bad signature" });
		} else {
			console.log("webhook signature is valid");
		}

		// this boolean property indicates this is a test event sent from the publisher dashboard
		const isTestEvent = event.is_test_event === true;
		if (isTestEvent) {
			console.log("*** This is a TEST webhook event ***");
			// you may wish to simply acknowledge a test event and return early
			return response.send();
		}

		// Handle the event
		switch (event.type) {
			case "order.ready":
				// this event is fired when a order is complete and ready to be printed
				// the payload contains information about the customer's order, including shipping information,
				// a link to the PDF, book dimensions, SKU, etc.
				const order = event.data;
				// Then define and call a method to handle the fulfilment of this order
				// This code should run asynchronously outside of this webhook endpoint
				// handleOrderReady(order)
				break;

			default:
				console.log(`Unhandled event type ${event.type}.`);
		}

		// Return a 200 response to acknowledge the receipt of the event as quickly as possible,
		// since Memento Yearbook retries the event if a response is not
		// sent within a reasonable time.

		// As such, any long running code (e.g. downloading a PDF) in the event handlers above
		// should be run asynchronously to prevent timeouts
		response.json({ message: "webhook handled" });
	}
);

app.listen(port, () => {
	console.log(`Example webhook client listening at http://localhost:${port}`);
});

/**
 * Determines if the webhook signature of the request is valid.
 * This allows you to verify that the events were sent by Memento Photobook
 * and not a third party.
 *
 * @param request
 * @param secret
 * @returns {boolean}
 */
function isValidSignature(request, secret) {
	const signature = request.header("Signature");
	const computedSignature = crypto
		.createHmac("sha256", secret)
		.update(request.body, "utf8")
		.digest("hex");
	console.log(`sig: ${signature} | computed: ${computedSignature}`)
	return signature === computedSignature;
}
