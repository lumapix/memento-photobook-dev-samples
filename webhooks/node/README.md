# Node.js webhook endpoint example

## Requirements

You should have Node v7.6+ installed + npm or yarn.

## Start the development server

`yarn` or `npm install`

`node index.js`
