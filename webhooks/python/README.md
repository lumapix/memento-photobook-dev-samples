# Python webhook endpoint example

## Requirements

You should have Python 3 and pip installed.

## Start the development server

`pip install -r requirements.txt`

`python main.py`
