# Example webhook client which uses Flask to expose a webhook endpoint
# to handle events from Memento Photobook

from flask import Flask, request, jsonify
import hmac
from hashlib import sha256

app = Flask(__name__)

@app.route('/webhook', methods = ['POST'])
def webhook_handler():
	event = request.get_json()

	# verify the request signature is valid
	# update the signing secret with the value in your publisher dashboard
	secret = 'whsec_IbtsPEAESJ2HuDzeC4r27SQb1Gh4qQpY'
	if not is_valid_signature(request, secret):
		return jsonify(message='Invalid Signature'), 422

	# this boolean property indicates this is a test event sent from the publisher dashboard
	is_test_event = event['is_test_event']
	if (is_test_event is True):
		# you may wish to simply acknowledge a test event and return early
		return jsonify(message='Webhook Handled'), 200

	# Handle the event
	if event['type'] == 'order.ready':
		# this event is fired when a order is complete and ready to be printed
    	# the payload contains information about the customer's order, including shipping information,
    	# a link to the PDF, book dimensions, SKU, etc.
		order = event['data']
		# Then define and call a method to handle the fulfilment of this order
		# This code should run asynchronously outside of this webhook endpoint
		# handleOrderReady(order)
	else:
		print('Unhandled event type {}'.format(event['type']))

	return jsonify(message='Webhook Handled'), 200

# Determines if the webhook signature of the request is valid.
# This allows you to verify that the events were sent by Memento Photobooks
# and not a third party.
def is_valid_signature(request, secret):
	signature = request.headers.get("Signature", None)
	payload = request.data.decode("utf-8")
	mac = hmac.new(
		secret.encode("utf-8"),
		msg=payload.encode("utf-8"),
		digestmod=sha256,
	)
	computed = mac.hexdigest()
	return signature == computed

if __name__ == '__main__':
    app.run(port=3001)
